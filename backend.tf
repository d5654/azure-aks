terraform {
  backend "azurerm" {
    resource_group_name  = "webinar-aubaypt-aks"
    storage_account_name = "webinaraks"
    container_name       = "wbr-aks-tfstate"
    key                  = "wb-aks.tfstate"
  }
}